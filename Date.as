package {
    [Ruffle(InstanceAllocator)]
    [Ruffle(CallHandler)]
    public dynamic class Date {
        public static const length:int = 7;


        prototype.setTime = function(t:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setTime(t);
        }
        prototype.valueOf = function():* {
            var d:Date = this;
            return d.AS3::valueOf();
        }
        prototype.toString = function():String {
            var d:Date = this;
            return d.AS3::toString();
        }
        prototype.toDateString = function():String {
            var d:Date = this;
            return d.AS3::toDateString();
        }
        prototype.toTimeString = function():String {
            var d:Date = this;
            return d.AS3::toTimeString();
        }
        prototype.toLocaleString = function():String {
            var d:Date = this;
            return d.AS3::toLocaleString();
        }
        prototype.toLocaleDateString = function():String {
            var d:Date = this;
            return d.AS3::toLocaleDateString();
        }
        prototype.toLocaleTimeString = function():String {
            var d:Date = this;
            return d.AS3::toLocaleTimeString();
        }
        prototype.toUTCString = function():String {
            var d:Date = this;
            return d.AS3::toUTCString();
        }
        prototype.toJSON = function(k:String):* {
            var d:Date = this;
            return d.AS3::toString();
        }
        prototype.getUTCFullYear = function():Number {
            var d:Date = this;
            return d.AS3::getUTCFullYear();
        }
        prototype.getUTCMonth = function():Number {
            var d:Date = this;
            return d.AS3::getUTCMonth();
        }
        prototype.getUTCDate = function():Number {
            var d:Date = this;
            return d.AS3::getUTCDate();
        }
        prototype.getUTCDay = function():Number {
            var d:Date = this;
            return d.AS3::getUTCDay();
        }
        prototype.getUTCHours = function():Number {
            var d:Date = this;
            return d.AS3::getUTCHours();
        }
        prototype.getUTCMinutes = function():Number {
            var d:Date = this;
            return d.AS3::getUTCMinutes();
        }
        prototype.getUTCSeconds = function():Number {
            var d:Date = this;
            return d.AS3::getUTCSeconds();
        }
        prototype.getUTCMilliseconds = function():Number {
            var d:Date = this;
            return d.AS3::getUTCMilliseconds();
        }
        prototype.getFullYear = function():Number {
            var d:Date = this;
            return d.AS3::getFullYear();
        }
        prototype.getMonth = function():Number {
            var d:Date = this;
            return d.AS3::getMonth();
        }
        prototype.getDate = function():Number {
            var d:Date = this;
            return d.AS3::getDate();
        }
        prototype.getDay = function():Number {
            var d:Date = this;
            return d.AS3::getDay();
        }
        prototype.getHours = function():Number {
            var d:Date = this;
            return d.AS3::getHours();
        }
        prototype.getMinutes = function():Number {
            var d:Date = this;
            return d.AS3::getMinutes();
        }
        prototype.getSeconds = function():Number {
            var d:Date = this;
            return d.AS3::getSeconds();
        }
        prototype.getMilliseconds = function():Number {
            var d:Date = this;
            return d.AS3::getMilliseconds();
        }
        prototype.getTimezoneOffset = function():Number {
            var d:Date = this;
            return d.AS3::getTimezoneOffset();
        }
        prototype.getTime = function():Number {
            var d:Date = this;
            return d.AS3::getTime();
        }
        prototype.setFullYear = function(year:* = undefined, month:* = undefined, day:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setFullYear(year, month, day);
        }
        prototype.setMonth = function(month:* = undefined, day:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setMonth(month, day);
        }
        prototype.setDate = function(day:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setDate(day);
        }
        prototype.setHours = function(hour:* = undefined, min:* = undefined, sec:* = undefined, ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setHours(hour, min, sec, ms);
        }
        prototype.setMinutes = function(min:* = undefined, sec:* = undefined, ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setMinutes(min, sec, ms);
        }
        prototype.setSeconds = function(sec:* = undefined, ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setSeconds(sec, ms);
        }
        prototype.setMilliseconds = function(ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setMilliseconds(ms);
        }
        prototype.setUTCFullYear = function(year:* = undefined, month:* = undefined, day:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCFullYear(year, month, day);
        }
        prototype.setUTCMonth = function(month:* = undefined, day:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCMonth(month, day);
        }
        prototype.setUTCDate = function(day:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCDate(day);
        }
        prototype.setUTCHours = function(hour:* = undefined, min:* = undefined, sec:* = undefined, ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCHours(hour, min, sec, ms);
        }
        prototype.setUTCMinutes = function(min:* = undefined, sec:* = undefined, ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCMinutes(min, sec, ms);
        }
        prototype.setUTCSeconds = function(sec:* = undefined, ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCSeconds(sec, ms);
        }
        prototype.setUTCMilliseconds = function(ms:* = undefined):Number {
            var d:Date = this;
            return d.AS3::setUTCMilliseconds(ms);
        }

        prototype.setPropertyIsEnumerable("setTime", false);
        prototype.setPropertyIsEnumerable("valueOf", false);
        prototype.setPropertyIsEnumerable("toString", false);
        prototype.setPropertyIsEnumerable("toDateString", false);
        prototype.setPropertyIsEnumerable("toTimeString", false);
        prototype.setPropertyIsEnumerable("toLocaleString", false);
        prototype.setPropertyIsEnumerable("toLocaleDateString", false);
        prototype.setPropertyIsEnumerable("toLocaleTimeString", false);
        prototype.setPropertyIsEnumerable("toUTCString", false);
        prototype.setPropertyIsEnumerable("toJSON", false);
        prototype.setPropertyIsEnumerable("getUTCFullYear", false);
        prototype.setPropertyIsEnumerable("getUTCMonth", false);
        prototype.setPropertyIsEnumerable("getUTCDate", false);
        prototype.setPropertyIsEnumerable("getUTCDay", false);
        prototype.setPropertyIsEnumerable("getUTCHours", false);
        prototype.setPropertyIsEnumerable("getUTCMinutes", false);
        prototype.setPropertyIsEnumerable("getUTCSeconds", false);
        prototype.setPropertyIsEnumerable("getUTCMilliseconds", false);
        prototype.setPropertyIsEnumerable("getFullYear", false);
        prototype.setPropertyIsEnumerable("getMonth", false);
        prototype.setPropertyIsEnumerable("getDate", false);
        prototype.setPropertyIsEnumerable("getDay", false);
        prototype.setPropertyIsEnumerable("getHours", false);
        prototype.setPropertyIsEnumerable("getMinutes", false);
        prototype.setPropertyIsEnumerable("getSeconds", false);
        prototype.setPropertyIsEnumerable("getMilliseconds", false);
        prototype.setPropertyIsEnumerable("getTimezoneOffset", false);
        prototype.setPropertyIsEnumerable("getTime", false);
        prototype.setPropertyIsEnumerable("setFullYear", false);
        prototype.setPropertyIsEnumerable("setMonth", false);
        prototype.setPropertyIsEnumerable("setDate", false);
        prototype.setPropertyIsEnumerable("setHours", false);
        prototype.setPropertyIsEnumerable("setMinutes", false);
        prototype.setPropertyIsEnumerable("setSeconds", false);
        prototype.setPropertyIsEnumerable("setMilliseconds", false);
        prototype.setPropertyIsEnumerable("setUTCFullYear", false);
        prototype.setPropertyIsEnumerable("setUTCMonth", false);
        prototype.setPropertyIsEnumerable("setUTCDate", false);
        prototype.setPropertyIsEnumerable("setUTCHours", false);
        prototype.setPropertyIsEnumerable("setUTCMinutes", false);
        prototype.setPropertyIsEnumerable("setUTCSeconds", false);
        prototype.setPropertyIsEnumerable("setUTCMilliseconds", false);


        public native function Date(year:* = undefined, month:* = undefined, day:* = undefined, hours:* = undefined, minutes:* = undefined, seconds:* = undefined, ms:* = undefined);

        public static native function parse(param1:*):Number;

        public static native function UTC(param1:*, param2:*, param3:* = 1, param4:* = 0, param5:* = 0, param6:* = 0, param7:* = 0, ... rest):Number;


        AS3 function valueOf():Number {
            return this.getTime();
        }

        AS3 native function toString():String;

        AS3 native function toDateString():String;

        AS3 native function toTimeString():String;

        AS3 native function toLocaleString():String;

        AS3 function toLocaleDateString():String {
            return this.toDateString();
        }

        AS3 native function toLocaleTimeString():String;

        AS3 native function toUTCString():String;

        AS3 native function getUTCDay():Number;

        AS3 native function getDay():Number;

        AS3 native function getTimezoneOffset():Number;


        AS3 native function getTime():Number;
        AS3 native function setTime(time:* = undefined):Number;


        AS3 native function getFullYear():Number;
        AS3 native function setFullYear(year:* = undefined, month:* = undefined, day:* = undefined):Number;

        AS3 native function getMonth():Number;
        AS3 native function setMonth(month:* = undefined, day:* = undefined):Number;

        AS3 native function getDate():Number;
        AS3 native function setDate(day:* = undefined):Number;

        AS3 native function getHours():Number;
        AS3 native function setHours(hour:* = undefined, min:* = undefined, sec:* = undefined, ms:* = undefined):Number;

        AS3 native function getMinutes():Number;
        AS3 native function setMinutes(min:* = undefined, sec:* = undefined, ms:* = undefined):Number;

        AS3 native function getSeconds():Number;
        AS3 native function setSeconds(sec:* = undefined, ms:* = undefined):Number;

        AS3 native function getMilliseconds():Number;
        AS3 native function setMilliseconds(ms:* = undefined):Number;


        AS3 native function getUTCFullYear():Number;
        AS3 native function setUTCFullYear(year:* = undefined, month:* = undefined, day:* = undefined):Number;

        AS3 native function getUTCMonth():Number;
        AS3 native function setUTCMonth(month:* = undefined, day:* = undefined):Number;

        AS3 native function getUTCDate():Number;
        AS3 native function setUTCDate(day:* = undefined):Number;

        AS3 native function getUTCHours():Number;
        AS3 native function setUTCHours(hour:* = undefined, min:* = undefined, sec:* = undefined, ms:* = undefined):Number;

        AS3 native function getUTCMinutes():Number;
        AS3 native function setUTCMinutes(min:* = undefined, sec:* = undefined, ms:* = undefined):Number;

        AS3 native function getUTCSeconds():Number;
        AS3 native function setUTCSeconds(sec:* = undefined, ms:* = undefined):Number;

        AS3 native function getUTCMilliseconds():Number;
        AS3 native function setUTCMilliseconds(ms:* = undefined):Number;


        public function get fullYear():Number {
            return this.getFullYear();
        }

        public function set fullYear(value:Number):void {
            this.setFullYear(value);
        }

        public function get month():Number {
            return this.getMonth();
        }

        public function set month(value:Number):void {
            this.setMonth(value);
        }

        public function get date():Number {
            return this.getDate();
        }

        public function set date(value:Number):void {
            this.setDate(value);
        }

        public function get hours():Number {
            return this.getHours();
        }

        public function set hours(value:Number):void {
            this.setHours(value);
        }

        public function get minutes():Number {
            return this.getMinutes();
        }

        public function set minutes(value:Number):void {
            this.setMinutes(value);
        }

        public function get seconds():Number {
            return this.getSeconds();
        }

        public function set seconds(value:Number):void {
            this.setSeconds(value);
        }

        public function get milliseconds():Number {
            return this.getMilliseconds();
        }

        public function set milliseconds(value:Number):void {
            this.setMilliseconds(value);
        }

        public function get fullYearUTC():Number {
            return this.getUTCFullYear();
        }

        public function set fullYearUTC(value:Number):void {
            this.setUTCFullYear(value);
        }

        public function get monthUTC():Number {
            return this.getUTCMonth();
        }

        public function set monthUTC(value:Number):void {
            this.setUTCMonth(value);
        }

        public function get dateUTC():Number {
            return this.getUTCDate();
        }

        public function set dateUTC(value:Number):void {
            this.setUTCDate(value);
        }

        public function get hoursUTC():Number {
            return this.getUTCHours();
        }

        public function set hoursUTC(value:Number):void {
            this.setUTCHours(value);
        }

        public function get minutesUTC():Number {
            return this.getUTCMinutes();
        }

        public function set minutesUTC(value:Number):void {
            this.setUTCMinutes(value);
        }

        public function get secondsUTC():Number {
            return this.getUTCSeconds();
        }

        public function set secondsUTC(value:Number):void {
            this.setUTCSeconds(value);
        }

        public function get millisecondsUTC():Number {
            return this.getUTCMilliseconds();
        }

        public function set millisecondsUTC(value:Number):void {
            this.setUTCMilliseconds(value);
        }

        public function get time():Number {
            return this.getTime();
        }

        public function set time(value:Number):void {
            this.setTime(value);
        }

        public function get timezoneOffset():Number {
            return this.getTimezoneOffset();
        }

        public function get day():Number {
            return this.getDay();
        }

        public function get dayUTC():Number {
            return this.getUTCDay();
        }
    }
}